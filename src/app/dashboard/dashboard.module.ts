import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { OrdersModule } from './orders/orders.module';
import { ServiceModule } from './service/service.module';
import { CouponModule } from './coupon/coupon.module';
import { AboutModule } from './about/about.module';
import { ContactModule } from './contact/contact.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ServiceModule,
    OrdersModule,
    CouponModule,
    AboutModule,
    ContactModule
  ]
})
export class DashboardModule { }
