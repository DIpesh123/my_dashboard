import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ServiceCategoryComponent } from './service-category/service-category.component';
import { ServiceListComponent } from './service-list/service-list.component';

const routes: Routes = [
  {path:'service-category',component:ServiceCategoryComponent},
  {path:'service-list',component:ServiceListComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceRoutingModule { }
