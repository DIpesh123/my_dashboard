import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddCustomComponent } from './add-custom/add-custom.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
  {path:'add-custom',component:AddCustomComponent},
  {path:'list',component:ListComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
