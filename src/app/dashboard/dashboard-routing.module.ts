import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutModule } from './about/about.module';
import { ContactModule } from './contact/contact.module';
import { CouponModule } from './coupon/coupon.module';
import { OrdersModule } from './orders/orders.module';
import { ServiceModule } from './service/service.module';

const routes: Routes = [
  {path:'orders',component:OrdersModule},
  {path:'service',component:ServiceModule},
  {path:'coupon',component:CouponModule},
  {path:'about',component:AboutModule},
  {path:'contact',component:ContactModule}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
